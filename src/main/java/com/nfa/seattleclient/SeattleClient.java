package com.nfa.seattleclient;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by barclakj on 11/02/2018.
 */
public class SeattleClient {
    private static Logger log = Logger.getLogger(SeattleClient.class.getCanonicalName());

    public static final String SEATTLE_KEY = "X-SEATTLE-KEY";
    public static final int RELOAD_INTERVAL = 30000;
    public static final int CONFIG_WAIT_INTERVAL = 100;

    private static Client client;
    private static String identity;

    public static String BASE_URL = null;
    public static String project = null;
    public static AtomicInteger nodeIndex = null;
    public static String key = null;
    public static final ExecutorService executor = Executors.newSingleThreadExecutor();

    private static Properties seattleProperties = new Properties();
    private static String oldproperties = null;

    private static Set<IConfigObserver> allObservers = new HashSet<>();

    private static Map<Integer, InetAddress> projectClients = new HashMap<>();

    static {
        identity = UUID.randomUUID().toString();
        client = ClientBuilder.newBuilder()
                .register(JacksonFeature.class)
                .build();

    }

    /**
     * Starts the background executor poll for configuration changes.
     */
    public static void start() {
        executor.execute(() -> {
            boolean cont = true;
            do {
                try {
                    if (getProject()!=null && getBaseUrl()!=null && getKey()!=null) {
                        try {
                            fetchConfig(getProject(), getKey());
                        } catch (ProcessingException e) {
                            log.warning("Unable to process request at: " + BASE_URL);
                            log.log(Level.WARNING,"Unable to process request: " + e.getMessage(), e);
                        }
                        Thread.sleep(RELOAD_INTERVAL);
                    } else {
                        // Waiting for config...
                        Thread.sleep(CONFIG_WAIT_INTERVAL);
                    }
                } catch (InterruptedException e) {
                    cont = false;
                }

            } while (cont);
        });
    }

    public static String getProject() {
        return project;
    }

    public static void setProject(String project) {
        SeattleClient.project = project;
    }

    public static String getKey() {
        return key;
    }

    public static void setKey(String key) {
        SeattleClient.key = key;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static void setBaseUrl(String baseUrl) {
        BASE_URL = baseUrl;
    }

    public static String getIdentity() {
        return identity;
    }

    public static void addObserver(IConfigObserver obs) {
        allObservers.add(obs);
        if (seattleProperties!=null && nodeIndex!=null) {
            obs.setConfig(seattleProperties);
            obs.setNodeIndex(nodeIndex.intValue());
            for(Integer id : projectClients.keySet()) {
                obs.addNode(projectClients.get(id), id);
            }
        }
    }

    public static void main(String[] args) {
        addObserver(new BasicObserver());
        setBaseUrl("http://localhost:8080/config/");
        setKey("passworxd");
        setProject("aproject");

        start();

        try {
            // uploadConfig("aproject", loadJSONData(new FileInputStream("sample.json")) , key);

            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Fetches configuration from report server.
     * @param project
     * @param key
     */
    public static void fetchConfig(String project, String key) {
        WebTarget target = client.target(BASE_URL).path(getProject() + "/" + identity);

        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON);
        builder.header(SEATTLE_KEY, key);
        Response response = builder.get();
        if (response.getStatus()==Response.Status.OK.getStatusCode()) {
            String responseData = response.readEntity(String.class);
            JSONObject jo = new JSONObject(responseData);
            if (log.isLoggable(Level.FINEST)) log.finest(jo.toString(4));
            loadProperties(jo);
            loadNodeIndex(jo);
            loadProjectClients(jo);
        } else {
            log.warning("Failed to load config for project " + project + " status: " + response.getStatus());
        }
    }

    /**
     * Uploads configuration.
     * @param project
     * @param jo
     * @param key
     */
    public static void uploadConfig(String project, JSONObject jo, String key) {
        WebTarget target = client.target(BASE_URL).path(getProject());

        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON);
        builder.header(SEATTLE_KEY, key);
        Response response = builder.put(Entity.text(jo.toString()));
        if (response.getStatus()==Response.Status.OK.getStatusCode()) {
            log.info("Config uploaded ok");
        } else {
            log.warning("Failed to upload config for project " + project + " status: " + response.getStatus());
        }
    }

    /**
     * Loads JSON data from input stream.
     * @param is
     * @return
     */
    public static JSONObject loadJSONData(InputStream is) {
        JSONObject jo = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] chunk = new byte[8192];
            int read = 0;
            while((read=is.read(chunk,0,8192))>0) {
                baos.write(chunk,0,read);
            }
            String data = new String(baos.toByteArray());
            jo = new JSONObject(data);
        } catch (JSONException e) {
            log.warning("Failed to convert data to JSON! " + e.getMessage());
        } catch (IOException e) {
            log.warning("Failed to read input stream: " + e.getMessage());
        } finally {
            return jo;
        }
    }

    /**
     * Loads JSON props into properties.
     * @param jo
     */
    public static void loadProperties(JSONObject jo) {
        boolean dataChanged = true;
        Properties props = new Properties();
        if (jo.has("properties")) {
            JSONArray ja = jo.getJSONArray("properties");
            if (oldproperties!=null) {
                if (oldproperties.equals(ja.toString())) {
                    // nothing changed
                    dataChanged = false;
                }
            }
            oldproperties = ja.toString();
            if (dataChanged) {
                for (int i = 0; i < ja.length();i++) {
                    JSONObject jo2 = ja.getJSONObject(i);
                    Set<String> keys = jo2.keySet();
                    for (String k : jo2.keySet()) {
                        props.put(k, jo2.getString(k));
                        log.info("Loaded " + k + " => " + jo2.getString(k));
                    }
                }
                seattleProperties = props;
                notifyObserversConfigChange();
            }
        }
    }

    /**
     * Loads node index.
     * @param jo
     */
    public static void loadNodeIndex(JSONObject jo) {
        if (jo.has("___client")) {
            int index = jo.getJSONObject("___client").getInt("index");
            if (nodeIndex==null || index!=nodeIndex.intValue()) {
                nodeIndex= new AtomicInteger(index);
                notifyObserversIndexDefined();
            }
        }
    }

    /**
     * Loads client details.
     * @param jo
     */
    public static void loadProjectClients(JSONObject jo) {
        Map<Integer, InetAddress> identifiedClients = new HashMap<>();
        Set<Integer> delClients = new HashSet<>();
        if (jo.has("___project")) {
            JSONArray ja = jo.getJSONObject("___project").getJSONArray("clients");

            for(int i=0;i<ja.length();i++) {
                JSONObject jo2 = ja.getJSONObject(i);
                String addr = jo2.getString("address");
                int clientIndex = jo2.getInt("index");
                try {
                    InetAddress address = InetAddress.getByName(addr);
                    identifiedClients.put(clientIndex, address);
                } catch (UnknownHostException e) {
                    log.warning("Cannot find host: " + addr);
                }
            }

            // add to the new clients list if its not
            // in the existing project clients
            identifiedClients.keySet().stream()
                    .forEach(id -> {
                        if (!projectClients.containsKey(id)) {
                            log.info("New client found: " + id + " -> " + identifiedClients.get(id));
                            allObservers.stream()
                                    .forEach(obs -> obs.addNode(identifiedClients.get(id), id));
                            projectClients.put(id, identifiedClients.get(id));
                        }
                    });
            // remove any clients which seem to have disappeared
            projectClients.keySet().stream()
                    .forEach(id -> {
                        if (!identifiedClients.containsKey(id)) {
                            delClients.add(id);
                            log.info("Need to remove: " + id);
                        }
                    });
            // and cleanup removed.
            delClients.stream()
                    .forEach(id -> {
                        allObservers.stream()
                                .forEach(obs -> obs.removeNode(projectClients.get(id),  id));
                        projectClients.remove(id);
                    });

            if (log.isLoggable(Level.FINE)) {
                projectClients.keySet().stream()
                        .forEach(id -> {
                            log.fine(id + " -> " + projectClients.get(id));
                        });
            }
        }
    }

    /**
     * Notifies observers of a config change.
     */
    private static void notifyObserversConfigChange() {
        log.info("Properties changed. Notifying observers!");
        allObservers.parallelStream()
                .forEach(o -> o.setConfig(seattleProperties));
    }

    /**
     * Notifies observers of a config change.
     */
    private static void notifyObserversIndexDefined() {
        log.info("Index updated... Notifying observers!");
        allObservers.parallelStream()
                .forEach(o -> o.setNodeIndex(nodeIndex.intValue()));
    }
}
