package com.nfa.seattleclient;

import java.net.InetAddress;
import java.util.Properties;

/**
 * Created by barclakj on 11/02/2018.
 */
public interface IConfigObserver {
    void setConfig(Properties p);
    void setNodeIndex(int index);
    void addNode(InetAddress address, int index);
    void removeNode(InetAddress address, int index);
}
