package com.nfa.seattleclient;

import java.net.InetAddress;
import java.util.Properties;

/**
 * Created by barclakj on 17/02/2018.
 */
public class BasicObserver implements IConfigObserver {
    @Override
    public void setConfig(Properties p) {
        for(Object prop : p.keySet()) {
            System.out.println(prop + " -> " + p.get(prop));
        }
    }


    @Override
    public void setNodeIndex(int index){
        System.out.println("Node index: " + index);
    }

    @Override
    public void addNode(InetAddress address, int index) {
        System.out.println("Node added: " + index);
    }

    @Override
    public void removeNode(InetAddress address, int index) {
        System.out.println("Node removed: " + index);
    }
}
